import { Component } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { PersonalInfomationService } from '../app/personal-infomation.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PersonalInfomationService]
})

export class AppComponent implements OnInit {
  title = 'app';
  user: any;
  statusMessage: string;

  constructor(private personalInfomationService: PersonalInfomationService,
    private toastr: ToastrService) {

  }

  
  // getUsers() {
  //   this.personalInfomationService.getJSON()
  //     .subscribe(users => {
  //       this.user = users;
  //       console.log('Thong tin la:', this.user); 
  //       this.toastr.success('Get JSON successfully');      
  //     }, (error) => {
  //       this.statusMessage = 'API failed';
  //       this.toastr.error(this.statusMessage);
  //     });

  // }


  ngOnInit() {

    // this.getUsers();
  }
}
