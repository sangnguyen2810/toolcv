import { TestBed, inject } from '@angular/core/testing';

import { PersonalInfomationService } from './personal-infomation.service';

describe('PersonalInfomationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PersonalInfomationService]
    });
  });

  it('should be created', inject([PersonalInfomationService], (service: PersonalInfomationService) => {
    expect(service).toBeTruthy();
  }));
});
