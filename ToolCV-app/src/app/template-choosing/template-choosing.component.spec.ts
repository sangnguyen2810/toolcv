import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateChoosingComponent } from './template-choosing.component';

describe('TemplateChoosingComponent', () => {
  let component: TemplateChoosingComponent;
  let fixture: ComponentFixture<TemplateChoosingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateChoosingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateChoosingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
