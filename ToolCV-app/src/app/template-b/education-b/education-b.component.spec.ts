import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationBComponent } from './education-b.component';

describe('EducationBComponent', () => {
  let component: EducationBComponent;
  let fixture: ComponentFixture<EducationBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
