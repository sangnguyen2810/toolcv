import { Component, OnInit } from '@angular/core';
import { PersonalInfomationService } from '../../personal-infomation.service';

@Component({
  selector: 'app-education-b',
  templateUrl: './education-b.component.html',
  styleUrls: ['./education-b.component.css']
})
export class EducationBComponent implements OnInit {

  name: string = '';
  major: string ='';
  gradutedTime: string = '';

  constructor(private personalInformationService: PersonalInfomationService) { }

  ngOnInit() {
    this.getDataFromURL();
  }

  loadDataFromService(data) {
    for (let i = 0; i < data.education.length; i++) {
      this.name = data.education[i].name;
      this.major = data.education[i].major;
      this.gradutedTime = data.education[i].gradutedTime;
    }

  }

  getDataFromURL() {
    this.personalInformationService.getJSON().subscribe((data) => {
      this.loadDataFromService(data);
    })
  }
}
