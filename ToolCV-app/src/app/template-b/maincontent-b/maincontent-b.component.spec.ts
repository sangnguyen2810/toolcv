import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaincontentBComponent } from './maincontent-b.component';

describe('MaincontentBComponent', () => {
  let component: MaincontentBComponent;
  let fixture: ComponentFixture<MaincontentBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaincontentBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaincontentBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
