import { Component, OnInit } from '@angular/core';
import { PersonalInfomationService } from '../../personal-infomation.service';


@Component({
  selector: 'app-maincontent-b',
  templateUrl: './maincontent-b.component.html',
  styleUrls: ['./maincontent-b.component.css']
})

export class MaincontentBComponent implements OnInit {

  name: string = '';
  currentPosition: string = '';
  phoneNumber: string = '';
  address: string = '';
  email: string = '';
  facebook: string = '';
  github: string = '';
  linkedin: string = '';
  portfolio: string = '';
  image: string = '';
  personalStatement: string = '';
  skill: string = '';
  englishLevel: string = '';
  nameSelected:boolean = false;
  currentPositionSelected: boolean = false;

  constructor(private personalInformationService: PersonalInfomationService) { }

  ngOnInit() {
    this.getDataFromURL();
  }

  loadDataFromService(data) {
    this.name = data.personalInfo.name;
    this.currentPosition = data.personalInfo.currentPosition;
    this.phoneNumber = data.personalInfo.phoneNumber;
    this.address = data.personalInfo.address;
    this.email = data.personalInfo.email;
    this.facebook = data.personalInfo.facebook;
    this.github = data.personalInfo.github;
    this.linkedin = data.personalInfo.linkedin;
    this.portfolio = data.personalInfo.portfolio;
    this.personalStatement = data.personalStatement;
    this.englishLevel = data.skillSummary.englishLevel;
    this.skill = data.skillSummary.skill;
  }

  getDataFromURL() {
    this.personalInformationService.getJSON().subscribe((data) => {
      this.loadDataFromService(data);
    })
  }

  nameSelect(): void {
    this.nameSelected = !this.nameSelected;
  }
  nameEnter(event: any) { // without type info
    this.nameSelected = !this.nameSelected;
    this.name = event;
  }

  currentpositionSelect() {
    this.currentPositionSelected = !this.currentPositionSelected;
  }
  currentpositionEnter(event: any) {
    this.currentPositionSelected = !this.currentPositionSelected;
    this.currentPosition = event;
  }

}
