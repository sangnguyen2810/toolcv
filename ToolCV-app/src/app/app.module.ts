import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule }    from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TemplateAComponent } from './template-a/template-a.component';
import { MaincontentComponent } from './template-a/maincontent/maincontent.component';
import { SidebarComponent} from './template-a/sidebar/sidebar.component';
import { TemplateBComponent } from './template-b/template-b.component';
import { HeaderComponent } from './template-b/header/header.component';
import { MaincontentBComponent } from './template-b/maincontent-b/maincontent-b.component';
import { EducationBComponent } from './template-b/education-b/education-b.component';
import { RouterModule, Routes } from '@angular/router';
import {TemplateAExperienceComponent} from './template-a/maincontent/template-a-experience/template-a-experience.component';
import { TemplateChoosingComponent } from './template-choosing/template-choosing.component';
import { SidebarEducationComponent } from './template-a/sidebar/sidebar-education/sidebar-education.component';
import { SidebarAchievementComponent } from './template-a/sidebar/sidebar-achievement/sidebar-achievement.component';

const appRoutes: Routes = [
  { path: '', component: TemplateChoosingComponent , pathMatch: 'full' },
  { path: 'template-a', component: TemplateAComponent },
  { path: 'template-b', component: TemplateBComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TemplateAComponent,
    MaincontentComponent,
    SidebarComponent,
    TemplateBComponent,
    HeaderComponent,
    MaincontentBComponent,
    EducationBComponent,
    TemplateAExperienceComponent,
    TemplateChoosingComponent,
    SidebarEducationComponent,
    SidebarAchievementComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({positionClass: 'toast-top-center'}),
    AngularFontAwesomeModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
