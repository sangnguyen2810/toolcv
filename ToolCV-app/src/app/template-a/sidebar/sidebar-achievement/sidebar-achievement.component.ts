import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar-achievement',
  templateUrl: './sidebar-achievement.component.html',
  styleUrls: ['./sidebar-achievement.component.css']
})
export class SidebarAchievementComponent implements OnInit {

  @Input() specialAchievements: any;
  nameSelected: boolean[] = [];
  placeSelected: boolean[] = [];
  timeSelected: boolean[] = [];
  achievementsDeleted: any;
  
  @Output() achievementsDeletedEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  nameSelect(number: any){
    this.nameSelected[number] = !this.nameSelected[number];
  }
  nameEnter(event: any, number: any){
    this.nameSelect(number);
    this.specialAchievements[number].name = event;
  }

  placeSelect(number: any){
    this.placeSelected[number] = !this.placeSelected[number];
  }
  placeEnter(event: any, number: any){
    this.placeSelect(number);
    this.specialAchievements[number].place = event;
  }

  timeSelect(number: any){
    this.timeSelected[number] = !this.timeSelected[number];
  }
  timeEnter(event: any, number: any){
    this.timeSelect(number);
    this.specialAchievements[number].time = event;
  }
  achivementDelete(number: any){
    if (confirm("Do you really want to delete this ?!?!")) {
      this.specialAchievements.splice(number, 1);
      this.achievementsDeleted = this.specialAchievements;
      this.changeAchievement();
    } 
  }

  changeAchievement(){
    this.achievementsDeletedEvent.emit(this.achievementsDeleted);
  }
  


}
