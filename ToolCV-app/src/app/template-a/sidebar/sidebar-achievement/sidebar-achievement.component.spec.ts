import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarAchievementComponent } from './sidebar-achievement.component';

describe('SidebarAchievementComponent', () => {
  let component: SidebarAchievementComponent;
  let fixture: ComponentFixture<SidebarAchievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarAchievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarAchievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
