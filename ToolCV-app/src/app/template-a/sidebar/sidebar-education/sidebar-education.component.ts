import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar-education',
  templateUrl: './sidebar-education.component.html',
  styleUrls: ['./sidebar-education.component.css']
})
export class SidebarEducationComponent implements OnInit {

  @Input() education: any;
  @Output() educationDeletedEvent = new EventEmitter<any>();

  nameSelected: boolean[] = [];
  majorSelected: boolean[] = [];
  gradutedTimeSelected: boolean[] = [];
  constructor() { }

  ngOnInit() {
  }

  majorSelect(number: any){
    this.majorSelected[number] = !this.majorSelected[number];
  }
  majorEnter(event: any, number: any){
    this.majorSelect(number);
    this.education[number].major = event;
  }

  nameSelect(number: any){
    this.nameSelected[number] = !this.nameSelected[number];
  }
  nameEnter(event: any, number: any){
    this.nameSelect(number);
    this.education[number].name = event;
  }

  gradutedTimeSelect(number: any){
    this.gradutedTimeSelected[number] = !this.gradutedTimeSelected[number];
  }
  gradutedTimeEnter(event: any, number: any){
    this.gradutedTimeSelect(number);
    this.education[number].gradutedTime = event;
  }
  expDelete(number: any){
    if (confirm("Do you really want to delete this ?!?!")) {
      this.education.splice(number, 1);
      this.changeEducation();
    } 
  }

  changeEducation(){
    this.educationDeletedEvent.emit(this.education);
  }
}
