import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarEducationComponent } from './sidebar-education.component';

describe('SidebarEducationComponent', () => {
  let component: SidebarEducationComponent;
  let fixture: ComponentFixture<SidebarEducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarEducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarEducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
