import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() phoneNum: string;
  @Input() address: string;
  @Input() facebook: string;
  @Input() email: string;
  @Input() github: string;
  @Input() linkedin: string;
  @Input() portfolio: string;

  @Input() education: any;
  @Input() specialAchievements: any;

  @Output() educationDeletedEvent = new EventEmitter<any>();
  @Output() achievementsDeletedEvent = new EventEmitter<any>();
  phoneSelected: boolean = false;
  addressSelected: boolean = false;
  emailSelected: boolean = false;
  facebookSelected: boolean = false;
  linkedinSelected: boolean = false;
  githubSelected: boolean = false;
  portfolioSelected: boolean = false;


  constructor() { }

  ngOnInit() {
  }


  educationSelect() {
    for (let i = 0; i < this.education.length; i++) {
      const element = this.education[i];
    }
  }

  achievementsSelect(){
    for (let i = 0; i < this.specialAchievements.length; i++) {
      const element = this.specialAchievements[i];
    }
  }

  phoneSelect(){
    this.phoneSelected = !this.phoneSelected;
  }
  phoneEnter(event: any){
    this.phoneSelect();
    this.phoneNum = event;
  }

  addressSelect(){
    this.addressSelected = !this.addressSelected;
  }
  addressEnter(event: any){
    this.addressSelect();
    this.address = event;
  }

  emailSelect(){
    this. emailSelected = !this. emailSelected;
  }
  emailEnter(event: any){
    this. emailSelect();
    this.email = event;
  }

  facebookSelect(){
    this.facebookSelected = !this.facebookSelected;
  }
  facebookEnter(event: any){
    this.facebookSelect();
    this.facebook = event;
  }

  linkedinSelect(){
    this.linkedinSelected = !this.linkedinSelected;
  }
  linkedinEnter(event: any){
    this.linkedinSelect();
    this.linkedin = event;
  }

  githubSelect(){
    this.githubSelected = !this.githubSelected;
  }
  githubEnter(event: any){
    this.githubSelect();
    this.github = event;
  }

  portfolioSelect(){
    this.portfolioSelected = !this.portfolioSelected;
  }
  portfolioEnter(event: any){
    this.portfolioSelect();
    this.portfolio = event;
  }

  receiveEducationDeleted($event){
    this.educationDeletedEvent.emit(this.education);
  }
  receiveAchievementDeleted($event){
    this.achievementsDeletedEvent.emit(this.specialAchievements);
  }
}
