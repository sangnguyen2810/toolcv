import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateAExperienceComponent } from './template-a-experience.component';

describe('TemplateAExperienceComponent', () => {
  let component: TemplateAExperienceComponent;
  let fixture: ComponentFixture<TemplateAExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateAExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateAExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
