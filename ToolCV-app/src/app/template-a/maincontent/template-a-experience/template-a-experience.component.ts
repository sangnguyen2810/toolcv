import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-template-a-experience',
  templateUrl: './template-a-experience.component.html',
  styleUrls: ['./template-a-experience.component.css']
})
export class TemplateAExperienceComponent implements OnInit {
  @Input() experience: any;
  @Input() responsibilityItemSelected: any[]=[];

  experienceNumberSelected: boolean[] = [];

  companyNumberSelected: boolean[] = [];

  projectNumberSelected: boolean[] = [];

  startTimeNumberSelected: boolean[] = [];

  endTimeNumberSelected: boolean[] = [];
  technicalSelected: boolean[] = [];
  projectDescriptionSelected: boolean[] = [];

  experienceResponsibility: boolean[] = [];

  experienceDeleted: any;
  responsibilityDeleted: any;

  @Output() experienceDeletedEvent = new EventEmitter<any>();
  @Output() responsibilityDeletedEvent = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    console.log(this.responsibilityItemSelected);
  }

  responsibilityItemEnter(event: any, number: any, itemNo: any) {
    this.responsibilityItemSelected[number][itemNo] = !this.responsibilityItemSelected[number][itemNo];
    this.experience[number].responsibility[itemNo] = event;

  }
  responsibilityItemSelect(number: any, itemNo: any) {
    this.responsibilityItemSelected[number][itemNo] = !this.responsibilityItemSelected[number][itemNo];

  }
  experienceEnter(event: any, number: any) { // without type info

    this.experienceNumberSelected[number] = !this.experienceNumberSelected[number];

    this.experience[number].position = event;
  }
  experienceSelect(number: any) { // without type info

    this.experienceNumberSelected[number] = !this.experienceNumberSelected[number];
  }


  companyEnter(event: any, number: any) { // without type info

    this.companyNumberSelected[number] = !this.companyNumberSelected[number];
    this.experience[number].companyName = event;
  }
  companySelect(number: any) { // without type info
    console.log(number);
    this.companyNumberSelected[number] = !this.companyNumberSelected[number];
  }

  projectEnter(event: any, number: any) { // without type info

    this.projectNumberSelected[number] = !this.projectNumberSelected[number];
    this.experience[number].projectName = event;
  }
  projectSelect(number: any) { // without type info
    this.projectNumberSelected[number] = !this.projectNumberSelected[number];
  }


  startTimeEnter(event: any, number: any) { // without type info

    this.startTimeNumberSelected[number] = !this.startTimeNumberSelected[number];
    this.experience[number].startTime = event;
  }
  startTimeSelect(number: any) { // without type info
    this.startTimeNumberSelected[number] = !this.startTimeNumberSelected[number];
  }


  endTimeEnter(event: any, number: any) { // without type info

    this.endTimeNumberSelected[number] = !this.endTimeNumberSelected[number];
    this.experience[number].endTime = event;
  }
  endTimeSelect(number: any) { // without type info
    this.endTimeNumberSelected[number] = !this.endTimeNumberSelected[number];
  }

  technicalEnter(event: any, number: any) { // without type info

    this.technicalSelected[number] = !this.technicalSelected[number];
    this.experience[number].technicalSkills = event;
  }
  technicalSelect(number: any) { // without type info
    this.technicalSelected[number] = !this.technicalSelected[number];
  }

  projectDescriptionEnter(event: any, number: any) { // without type info
    this.projectDescriptionSelected[number] = !this.projectDescriptionSelected[number];
    this.experience[number].projectDescription = event;
  }
  projectDescriptionSelect(number: any) { // without type info
    this.projectDescriptionSelected[number] = !this.projectDescriptionSelected[number];
  }

  experienceDelete(i: any) {
    if (confirm("Do you really want to delete this ?!?!")) {
      this.experience.splice(i, 1);
      this.experienceDeleted = this.experience;
      this.changeExperience();
      //console.log(this.experience);
    }
  }
  resDelete(i: any, j: any) {
    if (confirm("Do you really want to delete this ?!?!")) {
      this.experience[i].responsibility.splice(j, 1);
      this.responsibilityDeleted = this.experience[i].responsibility;
      this.changeReposibility();
    }
  }

  changeExperience(){
    this.experienceDeletedEvent.emit(this.experience);
  }
  changeReposibility(){
    this.responsibilityDeletedEvent.emit(this.responsibilityDeleted);
    //console.log(this.responsibilityDeleted);
  }
}
