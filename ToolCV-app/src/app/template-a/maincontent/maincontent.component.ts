import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { PACKAGE_ROOT_URL } from '@angular/core/src/application_tokens';
import { PersonalInfomationService } from '../../personal-infomation.service';

@Component({
  selector: 'app-maincontent',
  templateUrl: './maincontent.component.html',
  styleUrls: ['./maincontent.component.css']
})
export class MaincontentComponent implements OnInit {

  @Input() name: string ='';
  @Input() currentPosition: string = "";
  @Input() personalStatement: string = "";
  @Input() skillSummary: any={
    englishLevel: '',
    skill: '',
  };
  @Input() experience: any;
  @Input() responsibilityItemSelected: any[]=[];
  nameSelected: boolean = false;
  currentpostionSelected: boolean = false;
  profileSelected: boolean = false;
  summarySelected: boolean = false;
  englishSelected: boolean = false;

  @Output() experienceDeletedEvent = new EventEmitter<any>();
  @Output() responsibilityDeletedEvent = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    
  }

  nameSelect(): void {
    this.nameSelected = !this.nameSelected;
  }
  nameEnter(event: any) { // without type info
    this.nameSelected = !this.nameSelected;
    this.name = event;
  }


  currentpositionSelect() {
    this.currentpostionSelected = !this.currentpostionSelected;
  }
  currentpositionEnter(event: any) {
    this.currentpostionSelected = !this.currentpostionSelected;
    this.currentPosition = event;
  }

  profileSelect() {
    this.profileSelected = !this.profileSelected;
  }
  profileEnter(event: any) {
    this.profileSelected = !this.profileSelected;
    this.personalStatement = event;
  }

  summarySelect() {
    this.summarySelected = !this.summarySelected;
  }
  summaryEnter(event: any) {
    this.summarySelected = !this.summarySelected;
    this.skillSummary.skill = event;
  }
  englishSelect() {
    this.englishSelected = !this.englishSelected;
  }
  englishEnter(event: any) {
    this.englishSelected = !this.englishSelected;
    this.skillSummary.englishLevel = event;
  }

  receiveDeletedExperience($event){
    this.experienceDeletedEvent.emit(this.experience);
  }
  receiveDeletedResponsibility($event){
    this.responsibilityDeletedEvent.emit(this.responsibilityItemSelected);
    console.log(this.experience.responsibility);
  }
}
