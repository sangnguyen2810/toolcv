import { Component, OnInit } from '@angular/core';
import { PersonalInfomationService } from '../personal-infomation.service';


@Component({
  selector: 'app-template-a',
  templateUrl: './template-a.component.html',
  styleUrls: ['./template-a.component.css']
})
export class TemplateAComponent implements OnInit {

  name: string = '';
  currentPosition: string;
  personalStatement: string;
  skillSummary: any = {
    englishLevel: '',
    skill: ''
  }
  experience: any;
  experiencePositionSelected: boolean[] = [];
  companyNumberSelected: boolean[] = [];
  projectNumberSelected: boolean[] = [];
  startTimeNumberSelected: boolean[] = [];
  endTimeNumberSelected: boolean[] = [];
  technicalSelected: boolean[] = [];
  projectDescriptionSelected: boolean[] = [];
  experienceResponsibility: boolean[] = [];
  responsibilityItemSelected: any[]=[];

  phoneNum: string;
  address: string;
  facebook: string;
  email: string;
  github: string;
  linkedin: string;
  portfolio: string;
  imagePath: string;
  education: any;
  specialAchievements: any;

  

  constructor(private personalInformationService: PersonalInfomationService) { }

  ngOnInit() {
    this.getDataFromURL();
    
  }

  getDataFromURL() {
    this.personalInformationService.getJSON().subscribe((data) => {
      let data_info: any;
      data_info = data;
      this.name = data_info.personalInfo.name;
      this.currentPosition = data_info.personalInfo.currentPosition;
      this.personalStatement = data_info.personalStatement;
      this.skillSummary.englishLevel = data_info.skillSummary.englishLevel;
      this.skillSummary.skill = data_info.skillSummary.skill;
      this.experience = data_info.experience;
      
      this.phoneNum = data_info.personalInfo.phoneNumber;
      this.address =  data_info.personalInfo.address;
      this.email = data_info.personalInfo.email;
      this.github = data_info.personalInfo.github;
      this.facebook =  data_info.personalInfo.facebook;
      this.linkedin = data_info.personalInfo.linkedin;
      this.portfolio = data_info.personalInfo.portfolio;
      this.education = data_info.education;
      this.specialAchievements = data_info.specialAchievements;
      this.getNumberOfExperience();
    })
  }

  getNumberOfExperience() {
    for (let i = 0; i < this.experience.length; i++) {
      this.experiencePositionSelected[i] = false;
      this.companyNumberSelected[i] = false;
      this.projectNumberSelected[i] = false;
      this.startTimeNumberSelected[i] = false;
      this.endTimeNumberSelected[i] = false;
      this.technicalSelected[i] = false;
      this.projectDescriptionSelected[i] = false;
      this.experienceResponsibility[i] = false;

      let exp: any[] = [];

      for (let j = 0; j < this.experience[i].responsibility.length; j++) {
        exp.push(false);
      }
      this.responsibilityItemSelected.push(exp);

    }
  }

  receiveDeletedExperience($event){
    this.experience = $event;
    console.log(this.experience);
  }
  receiveDeletedResponsibility($event){
    // this.responsibilityItemSelected = $event;
    // console.log(this.responsibilityItemSelected);
  }

  receiveEducationDeleted($event){
    this.education = $event;
    console.log(this.education);
  }

  receiveAchievementDeleted($event){
    this.specialAchievements = $event;
    console.log(this.specialAchievements);
  }




}
